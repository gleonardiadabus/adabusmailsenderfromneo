package adabusmailsenderfromneo.thread;

import com.asc.enterprise.thirdpartyconnector.ComparisonMode;
import com.asc.enterprise.thirdpartyconnector.Conversation;
import com.asc.enterprise.thirdpartyconnector.Employee;
import com.asc.enterprise.thirdpartyconnector.ParameterFilter;
import com.asc.enterprise.thirdpartyconnector.SearchOptions;
import adabusmailsenderfromneo.service.DatabaseService;
import adabusmailsenderfromneo.properties.MailSenderService;
import adabusmailsenderfromneo.properties.MiscProperties;
import adabusmailsenderfromneo.service.WsdlService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class MailSenderFromNeoThread extends TimerTask {

    Properties prop = new Properties();
    InputStream input = null;
    private DatabaseService dbService = null;
    private MailSenderService mailService = null;
    private MiscProperties miscProperties = null;
    private WsdlService wsdlService = null;
    private static final Logger logger = LogManager.getRootLogger();

    private boolean stop;

    public MailSenderFromNeoThread() {
        super();
        loadProperties();
        stop = false;
    }

    private void loadProperties() {
        try {
            input = new FileInputStream("cfg/AdabusMailSenderFromNeo.properties");
            // load a properties file
            prop.load(input);

            dbService = new DatabaseService(prop);
            logger.info("Database Properties: \n" + dbService.toString());

            mailService = new MailSenderService(prop);
            logger.info("Mail Properties: \n" + mailService.toString());

            miscProperties = new MiscProperties(prop);
            logger.info("Misc Properties: \n" + miscProperties.toString());

            wsdlService = new WsdlService(prop);
            logger.info("Wsdl Properties: \n" + wsdlService.toString());

        } catch (IOException ex) {
            logger.error("An error occured reading property:" + ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error("An error occured closing property file:" + e);
                }
            }
        }
    }

    public void run() {
            try {
                List<Employee> allParticipants = wsdlService.getAllParticipants();
                List<Conversation> lastConversations = wsdlService.getLastConversations(getOptionsList());
                lastConversations.forEach((Conversation conversation) -> {
                    Set<Employee> conversationEmployees = new HashSet<Employee>();
                    conversation.getParticipants().forEach((conversationParticipant) -> {
                        if (conversationParticipant.getEmployeeId() != null) {
                            Optional<Employee> conversationEmployee = allParticipants.stream().filter((participant) -> {
                                return participant.getExtensions() != null && isPhoneNumberContained(participant.getExtensions().getExtension(),conversationParticipant.getPhoneNumber());
                 
                            }).findFirst();
                            if (conversationEmployee.isPresent()) {
                                conversationEmployees.add(conversationEmployee.get());
                            }
                        }
                    });
                    if (conversationEmployees.isEmpty()) {
                        logger.warn("Non è stato trovato participant per la conversation " + conversation.getConversationId());
                    } else {
                        List<String> conversationIds = new ArrayList<>();
                        logger.info("Trovato participant per la conversation " + conversation.getConversationId());
                        String urlWaveLink = miscProperties.getSendsMail() ? wsdlService.getUrlWaveLink(conversation.getConversationId()) : "";
                        conversationEmployees.forEach((employee) -> {
                            Boolean updateConvStatus = false;
                            if (urlWaveLink.isEmpty()) {
                                updateConvStatus = true;
                            } else if(miscProperties.getSendsMail()){
                                logger.info("Send mail for employee "+ employee.getLoginName());
                                updateConvStatus = mailService.sendMail(conversation, employee, urlWaveLink);
                            }
                            if (updateConvStatus) {
                                conversationIds.add(conversation.getConversationId());
                            }

                        });
                        if (!conversationIds.isEmpty()) {
                            dbService.updateConversationStatus(conversationIds);
                        } else {
                            logger.warn("Tutte le chiamate sono andate in errore! Il sistema riprovera' il flow nel giro successivo.");
                        }
                    }
                });
            } catch (Exception ex) {
                logger.error("CRITICAL - Process interrupted...", ex);
                return;
            }
        logger.info("End of execution");
    }

    public void stop() {
        stop = true;
    }

    public SearchOptions getOptionsList() {
        SearchOptions searchOpt = new SearchOptions();
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DAY_OF_MONTH, miscProperties.getDaysForConversations() * -1);
        searchOpt.setStartTimeMin(start.getTimeInMillis());

        searchOpt.setStartTimeMax(new Date().getTime());
        searchOpt.setStartTimeMode(ComparisonMode.LIES_BETWEEN_MIN_AND_MAX);
        ParameterFilter parameterFilter = new ParameterFilter();
        parameterFilter.setParameterId(dbService.getConversationField());
        parameterFilter.setParameterMode(ComparisonMode.IS_NOT_EQUAL);
        parameterFilter.setParameterValue("OK");
        searchOpt.getParameters().add(parameterFilter);
        return searchOpt;
    }

    public boolean isPhoneNumberContained(List<String> extensions, String phoneNumber) {
        if(extensions == null || extensions.isEmpty() || phoneNumber == null || phoneNumber.isEmpty()){
            return false;
        }
        boolean present = extensions.stream().filter(extension->{
            return phoneNumber.equals(extension) || checkPattern(extension, phoneNumber) || checkPattern(phoneNumber, extension);
            
        }).findAny().isPresent();
        return present;
    }

    private boolean checkPattern(String sourceString, String compareString) {
         Pattern prefixPlus = Pattern.compile("^(\\+\\d{1,2})?"+compareString);
             Pattern prefix = Pattern.compile("^(\\d{1,2})?"+compareString);
             Pattern zero = Pattern.compile("0"+compareString);      
             Pattern prefixAndZeroAndPlus = Pattern.compile("^\\+(?:[\\d]{2})"+"0"+compareString);  
             Pattern prefixAndZero = Pattern.compile("^(?:[\\d]{2})"+"0"+compareString);    
             
             return 
                     (prefix.matcher(sourceString).find() || prefixPlus.matcher(sourceString).find() || zero.matcher(sourceString).find() || 
                             prefixAndZero.matcher(sourceString).find() || prefixAndZeroAndPlus.matcher(sourceString).find());
    }

}
