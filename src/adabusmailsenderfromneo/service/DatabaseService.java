package adabusmailsenderfromneo.service;

import adabusmailsenderfromneo.properties.CfgProperties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class DatabaseService extends CfgProperties {

    private String databaseHost;
    private String databasePort;
    private String databaseSchema;
    private String conversationTable;
    private String conversationField;
    private String databaseUser;
    private String databasePassword;

    public DatabaseService(Properties prop) {
        super(prop);
    }

    @Override
    public void fillProperties(Properties prop) {
        this.databaseHost = prop.getProperty("db_host");
        this.databasePort = prop.getProperty("db_port");
        this.databaseSchema = prop.getProperty("db_schema");
        this.conversationTable = prop.getProperty("db_table");
        this.conversationField = prop.getProperty("db_field");
        this.databaseUser = prop.getProperty("db_username");
        this.databasePassword = prop.getProperty("db_pwd");
    }

    public String getDatabaseHost() {
        return databaseHost;
    }

    public String getDatabasePort() {
        return databasePort;
    }

    public String getDatabaseSchema() {
        return databaseSchema;
    }

    public String getConversationTable() {
        return conversationTable;
    }

    public String getConversationField() {
        return conversationField;
    }

    public String getDatabaseUser() {
        return databaseUser;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public void updateConversationStatus(List<String> conversationIds) {
        getLogger().info("Inizio metodo per aggiornare lo stato delle conversazioni per le chiamate per le quali e' stata inviata la mail");
        String conversationIdsString = conversationIds.stream().map((t) -> {
            return "'" + t + "'";
        }).collect(Collectors.joining(","));
        getLogger().info("Verranno aggiornate " + conversationIds.size() + " conversazioni con i seguenti id: " + conversationIdsString);
        String url = "jdbc:postgresql://" + getDatabaseHost() + ":" + getDatabasePort() + "/" + getDatabaseSchema();
        try {
            Connection con = DriverManager.getConnection(url, getDatabaseUser(), getDatabasePassword());
            Statement st = con.createStatement();
            int executeUpdate = st.executeUpdate(
                    "UPDATE " + getDatabaseSchema() + "." + getConversationTable()
                            + " SET " + getConversationField() + " = 'OK' WHERE id::text IN ("+conversationIdsString+")");
            getLogger().info("Aggiornamento completato con successo!");

        } catch (SQLException ex) {

            getLogger().error(ex.getMessage(), ex);
        }
    }
}
