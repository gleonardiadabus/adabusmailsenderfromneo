package adabusmailsenderfromneo.service;

import com.asc.enterprise.thirdpartyconnector.ASCWebService;
import com.asc.enterprise.thirdpartyconnector.ASCWebServiceService;
import com.asc.enterprise.thirdpartyconnector.CodecType;
import com.asc.enterprise.thirdpartyconnector.Conversation;
import com.asc.enterprise.thirdpartyconnector.ConversationDownloadWebServiceResult;
import com.asc.enterprise.thirdpartyconnector.ConversationExportState;
import com.asc.enterprise.thirdpartyconnector.ConversationSearchWebServiceResult;
import com.asc.enterprise.thirdpartyconnector.Employee;
import com.asc.enterprise.thirdpartyconnector.EmployeeExportWebServiceResult;
import com.asc.enterprise.thirdpartyconnector.SearchOptions;
import adabusmailsenderfromneo.properties.CfgProperties;
import com.asc.enterprise.thirdpartyconnector.EmployeeSearchOptions;
import com.asc.enterprise.thirdpartyconnector.ExportFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class WsdlService extends CfgProperties {

    private String wsdlUsername;
    private String wsdlPwd;

    public WsdlService(Properties prop) {
        super(prop);
    }

    @Override
    public void fillProperties(Properties prop) {
        this.wsdlUsername = prop.getProperty("ws_user");
        this.wsdlPwd = prop.getProperty("ws_pwd");
    }

    public String getWsdlUsername() {
        return wsdlUsername;
    }

    public String getWsdlPwd() {
        return wsdlPwd;
    }

    public List<Employee> getAllParticipants() {
        getLogger().info("Tentativo di ottenere la lista dei Participant per il tenant");
        ASCWebService ws = new ASCWebServiceService().getASCWebServicePort();
        EmployeeExportWebServiceResult exportEmployee = ws.exportEmployee(getWsdlUsername(), getWsdlPwd(), new EmployeeSearchOptions());
        getLogger().info("Il numero di participant trovati e' " + exportEmployee.getEmployees().getEmployee().size());
        return exportEmployee.getEmployees().getEmployee();
    }

    public List<Conversation> getLastConversations(SearchOptions opt) {
        getLogger().info("Tentativo di raggiungere il Neo attraverso ws...");
        ASCWebService ws = new ASCWebServiceService().getASCWebServicePort();
        ConversationSearchWebServiceResult searchConversations = ws.searchConversations(getWsdlUsername(), getWsdlPwd(), opt);

        getLogger().info("Il numero di conversation e':" + searchConversations.getConversations().getConversationCount());
        final List<Conversation> results = new ArrayList();
        return searchConversations.getConversations().getConversation();

    }

    public String getUrlWaveLink(String conversationId) {
        try {
            String link = "";
            ASCWebService ws = new ASCWebServiceService().getASCWebServicePort();
            ConversationDownloadWebServiceResult exportConversation = ws.exportConversation(wsdlUsername, wsdlPwd, conversationId, true, CodecType.PCM_16, false, ExportFormat.WAVE);
            getLogger().info("link id [" + conversationId + "] state [" + exportConversation.getExportState() + "]");
            Integer counter = 0;
            if (exportConversation.getExportState().equals(ConversationExportState.ERROR)) {
                getLogger().error("Impossibile scaricare il file wave :" + exportConversation.getResultLog());
            } else {

                exportConversation = ws.exportConversation(wsdlUsername, wsdlPwd, conversationId, false, CodecType.PCM_16, false,ExportFormat.WAVE);
                getLogger().info("link id [" + conversationId + "] state [" + exportConversation.getExportState() + "]");

                
                
                while ((ConversationExportState.REQUESTED.equals(exportConversation.getExportState()) || ConversationExportState.EXPORTING.equals(exportConversation.getExportState())) && counter < 50) {
                    Thread.sleep(1000 * 4);
                    exportConversation = ws.exportConversation(wsdlUsername, wsdlPwd, conversationId, false, CodecType.PCM_16, false,ExportFormat.WAVE);
                    counter++;
                    getLogger().info("link id [" + conversationId + "] counter [" + counter + "/" + 50 + "] "
                            + "state [" + exportConversation.getExportState() + "] "
                            + "result code [" + exportConversation.getResultCode() + "] "
                            + "result log [" + exportConversation.getResultLog() + "]");
                }
                getLogger().info("link id [" + conversationId + "]  state [" + exportConversation.getExportState() + "] result code [" + exportConversation.getResultCode() + "] result log [" + exportConversation.getResultLog() + "]");

                if (ConversationExportState.FINISHED.equals(exportConversation.getExportState())) {
                    link = exportConversation.getDownloadURL();
                } else if (ConversationExportState.ERROR.equals(exportConversation.getExportState())) {
                    getLogger().error("Impossibile scaricare il file wave :" + exportConversation.getResultLog());
                }
            }
            return link;
        } catch (Exception e) {
            getLogger().error("Impossibile scaricare il wave per la conversation " + conversationId, e);
            return "";
        }
    }

//    public static SearchOptions getOptions(String number) {
//        SearchOptions searchOpt = new SearchOptions();
//
//        ParticipantFilter participant = new ParticipantFilter();
//        //<phoneNumber>Engineering</phoneNumber>
//        //<phoneNumber>consel</phoneNumber>
//        participant.setPhoneCompare(number);
//        participant.setPhoneMode(ComparisonMode.IS_EQUAL);
//        searchOpt.setCalled(participant);
//        return searchOpt;
//    }
}
