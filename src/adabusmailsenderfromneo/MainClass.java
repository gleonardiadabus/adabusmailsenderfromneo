package adabusmailsenderfromneo;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import adabusmailsenderfromneo.thread.MailSenderFromNeoThread;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Timer;

/**
 * @author giuseppe
 */
public class MainClass {

    private static final Logger logger = LogManager.getRootLogger();
    private static final Timer timer = new Timer();
    private static boolean stop = false;
    private static MailSenderFromNeoThread tMainThread;


    public static void main(String[] args) {
        if (args.length < 1) {
            logger.error("Adabus Email Export ERROR: missing parameters");
        } else {
            logger.info("Adabus Email Export started!");
            if ("start".equals(args[0]))
                start();
            else if ("stop".equals(args[0]))
                stop();
        }
    }

    public static void start() {

        logger.info("Start Application INFO");

         tMainThread = new MailSenderFromNeoThread();
        timer.schedule(tMainThread, 0, 60000);
        while (!stop) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                logger.error("CRITICAL - Process interrupted...", ex);
                //stop all threads
                closeAll();
                return;
            }
        }
        logger.info("End Application INFO");
        closeAll();

    }

    private static void closeAll() {
        tMainThread.stop();
    }

    public static void stop() {
        logger.info("Stopping Adabus Email Export");
        stop = true;
    }


}
