
package adabusemailexport;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncodingAttributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailFileWriter {

    private static final Logger logger = LogManager.getRootLogger();
    private List<String> messageContent;
    private List<File> attachFiles;
    private String subject;
    private String fileName;


    private EmailFileWriter() {
        this.messageContent = new ArrayList<>();
        this.attachFiles = new ArrayList<>();
    }

    public EmailFileWriter(String subject) {
        this();
        this.subject = subject;
    }

    public List<String> getMessageContent() {
        return messageContent;
    }

    public List<File> getAttachFiles() {
        return attachFiles;
    }

    public void addMessageContent(String messageContent) {
        this.messageContent.add(messageContent);
    }

    public void addAttachFile(File attachFile) {
        this.attachFiles.add(attachFile);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean write(Properties prop) {
        try {
            if(getMessageContentString().isEmpty()){
                logger.error("Invalid body message!:"+getSubject());
                return false;
            }
            if(!new File(prop.getProperty("tempPath") + this.getName() + ".mp3").exists()){
                logger.error("Invalid file in message!:"+getSubject());
                return false;
            }
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.newDocument();
            Element calldata = doc.createElement("Calldata");
            doc.appendChild(calldata);
            for (int i = 0; i < DataType.values().length; i++) {
                calldata.appendChild(buildElement(doc, DataType.values()[i]));
            }


            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            FileOutputStream fos = new FileOutputStream(new File(prop.getProperty("tempPath") + getName() + ".xml"));
            StreamResult res = new StreamResult(fos);
            transformer.transform(source, res);
            fos.close();
            File sourceWav = new File(prop.getProperty("tempPath") + this.getName() + ".mp3");
            File target = new File(prop.getProperty("tempPath") + getName() + ".wav");
            AudioAttributes audio = new AudioAttributes();
            audio.setCodec("pcm_s16le");
            audio.setBitRate(new Integer(prop.getProperty("bitrate")));
            audio.setChannels(new Integer(prop.getProperty("channels_nr")));
            audio.setSamplingRate(new Integer(prop.getProperty("sampling")));
            EncodingAttributes attrs = new EncodingAttributes();
            attrs.setFormat("wav");
            attrs.setAudioAttributes(audio);
            Encoder encoder = new Encoder();
            encoder.encode(sourceWav, target, attrs);
            Files.move(
                    Paths.get(prop.getProperty("tempPath") + this.getName() + ".wav"),
                    Paths.get(prop.getProperty("importPath") + this.getName() + ".wav")
                    , StandardCopyOption.REPLACE_EXISTING);

            Files.move(
                    Paths.get(prop.getProperty("tempPath") + this.getName() + ".xml"),
                    Paths.get(prop.getProperty("importPath") + this.getName() + ".xml")
                    , StandardCopyOption.REPLACE_EXISTING);

            Files.delete(Paths.get(prop.getProperty("tempPath") + this.getName() + ".mp3"));


        } catch (Exception e) {
            logger.error("An error occured writing xml:", e);
            return false;
        }
    return true;
    }

    private Element buildElement(Document doc, DataType dataType) throws ParseException {
        Element element = doc.createElement(dataType.getLabel());
        element.appendChild(doc.createTextNode(fromRegex(dataType)));
        return element;
    }

    public String fromRegex(DataType dataType) throws ParseException {
        Pattern pattern = Pattern.compile(dataType.getRegexp());
        Matcher subjectMatcher = pattern.matcher(getSubject());
        Matcher messageMatcher = pattern.matcher(getMessageContentString());
        String result = "";
        switch (dataType) {
            case START_TIME_TEXT:
                if (messageMatcher.find()) {
                    result = messageMatcher.group().substring(5).trim();
                }
                return result.trim();
            case START_TIME:
                if (messageMatcher.find()) {
                    String dateTime = messageMatcher.group().substring(5).trim();
                    SimpleDateFormat fromMailFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat toXmlFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    result = toXmlFormat.format(fromMailFormat.parse(dateTime));
                }
                return result.trim();
            case CALLED_NUMBER:
                if (subjectMatcher.find() && subjectMatcher.find()) {
                    result = subjectMatcher.group(1);
                }
                return result.trim();
            case CALLER_NUMBER:
                if (subjectMatcher.find()) {
                    result = subjectMatcher.group(0);
                }
                return result.trim();
            case FILE_NAME:
                if (messageMatcher.find()) {
                    result = messageMatcher.group().substring(5);
                    return result.trim();
                }
            default:
                return result;
        }
    }

    public String getMessageContentString() {
        if (getMessageContent().size() == 1) {
            return getMessageContent().get(0).replace("\n", "").replace("\r", "");
        }
        StringJoiner joiner = new StringJoiner(" ");
        getMessageContent().stream().forEach(x -> joiner.add(x));
        return joiner.toString().replace("\n", "").replace("\r", "");
    }

    public String getName() {
        if (fileName == null || fileName.isEmpty()) {
            fileName = new SimpleDateFormat("YYYYmmdd_hhMMss").format(new Timestamp(System.currentTimeMillis()));
        }
        return fileName;
    }

    enum DataType {
        START_TIME("StartTime", "Date: \\d{4}\\-\\d{2}\\-\\d{2} \\d{2}\\:\\d{2}\\:\\d{2}"),
        START_TIME_TEXT("StartTimeText", "Date: \\d{4}\\-\\d{2}\\-\\d{2} \\d{2}\\:\\d{2}\\:\\d{2}"),
        CALLED_NUMBER("CalledNumber", "(?<=\\<)(.*?)(?=\\>)"),
        CALLER_NUMBER("CallerNumber", "(?<=\\<)(.*?)(?=\\>)"),
        FILE_NAME("NomeFileAudio", "File.*");

        private final String regexp;
        private final String label;

        DataType(String label, String regexp) {
            this.regexp = regexp;
            this.label = label;
        }

        public String getRegexp() {
            return regexp;
        }

        public String getLabel() {
            return label;
        }
    }

}
