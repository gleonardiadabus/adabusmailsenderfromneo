package adabusemailexport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class ApachePOIExcelWrite {
    private static final Logger logger = LogManager.getRootLogger();
    private static ApachePOIExcelWrite istance = null;

    private ApachePOIExcelWrite() {
        //noop
    }

    public static ApachePOIExcelWrite getIstance() {
        if (istance == null) {
            istance = new ApachePOIExcelWrite();
        }
        return istance;
    }

    public void produceXls(List<EmailFileWriter> list, String excelFilePath) {
        Workbook workbook = null;
        Sheet sheet = null;
        int rowNum = 0;

        if (new File(excelFilePath).exists()) {
            logger.debug("File exists...read last row");
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(new File(excelFilePath));
                workbook = new HSSFWorkbook(inputStream);
                sheet = workbook.getSheetAt(workbook.getActiveSheetIndex());
                rowNum = sheet.getLastRowNum() + 1;
            } catch (Exception e) {
                logger.error("An error occured on open xsl file", e);
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    logger.error("An error occured on close xsl file", e);
                }
            }
            logger.debug("Last row in xsl is " + rowNum);
        } else {
            logger.debug("File doesn't exist...create new file");
            workbook = createHeader();
            rowNum = 1;
            sheet = workbook.getSheetAt(workbook.getActiveSheetIndex());
        }

        logger.debug("Write processed email in xsl");
        for (EmailFileWriter email : list) {
            logger.debug("Processing->"+rowNum+", Message->"+email.getMessageContentString());
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;

            Cell idCell = row.createCell(colNum++);
            idCell.setCellValue(rowNum);

            Cell subjectCell = row.createCell(colNum++);
            subjectCell.setCellValue(email.getSubject());

            Cell dateCell = row.createCell(colNum++);
            try {
                dateCell.setCellValue(email.fromRegex(EmailFileWriter.DataType.START_TIME_TEXT));
            } catch (ParseException e) {
                logger.error("Error in parsing Start Time", e);
                dateCell.setCellValue("<UNDEFINED>");
            }

            Cell fileCell = row.createCell(colNum++);
            try {
                fileCell.setCellValue(email.fromRegex(EmailFileWriter.DataType.FILE_NAME));
            } catch (ParseException e) {
                logger.error("Error in parsing Start Time", e);
                fileCell.setCellValue("<UNDEFINED>");
            }
        }
        logger.debug("Save into file");

        try {
            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            workbook.close();
        } catch (Exception e) {
            logger.error("An error writingxsl file", e);
        }

        logger.debug("xsl saved!!");
    }

    private Workbook createHeader() {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Email processed");
        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.MEDIUM);
        style.setBorderTop(BorderStyle.MEDIUM);
        style.setBorderRight(BorderStyle.MEDIUM);
        style.setBorderLeft(BorderStyle.MEDIUM);
        style.setFillBackgroundColor(IndexedColors.DARK_GREEN.getIndex());
        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());


        Row header = sheet.createRow(0);
        int colNum = 0;
        Cell idCell = header.createCell(colNum++);
        idCell.setCellValue("Id");
        idCell.setCellStyle(style);

        Cell subjectCell = header.createCell(colNum++);
        subjectCell.setCellValue("Subject");
        subjectCell.setCellStyle(style);

        Cell dateCell = header.createCell(colNum++);
        dateCell.setCellValue("Date");
        dateCell.setCellStyle(style);

        Cell fileCell = header.createCell(colNum++);
        fileCell.setCellValue("File Name");
        fileCell.setCellStyle(style);
        return workbook;
    }
}
