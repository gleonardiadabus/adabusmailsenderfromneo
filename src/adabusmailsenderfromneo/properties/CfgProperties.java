package adabusmailsenderfromneo.properties;

import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class CfgProperties {
    protected Logger logger;

    public CfgProperties(Properties prop){
        fillProperties(prop);
        logger = LogManager.getRootLogger();
    }

    public abstract void fillProperties(Properties prop);

    public Logger getLogger() {
        return logger;
    }
}
