package adabusmailsenderfromneo.properties;

import com.asc.enterprise.thirdpartyconnector.Conversation;
import com.asc.enterprise.thirdpartyconnector.ConversationParameter;
import com.asc.enterprise.thirdpartyconnector.Employee;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.logging.log4j.Logger;

public class MailSenderService extends CfgProperties {

    private Properties mailProperties;
    private final Session mailSession;
    private String mailAddress;
    private String mailHost;
    private String user;
    private String password;
    private String mailPort;
    private String wsdlHost;
    private String customcpTicket;
    private boolean includeAttachment;

    public MailSenderService(Properties prop) {
        super(prop);
        mailProperties = new Properties();
        mailProperties.put("mail.smtp.from", mailAddress);
        mailProperties.put("mail.smtp.host", mailHost);
        mailProperties.put("mail.smtp.port", mailPort);
        mailProperties.put("mail.smtp.auth", getUser() != null && !"".equals(getUser()));
        mailProperties.put("mail.smtp.socketFactory.port", mailPort);
//        mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        mailProperties.put("mail.smtp.socketFactory.fallback", "false");
        mailProperties.put("mail.smtp.starttls.enable", "false");

        mailSession = Session.getInstance(mailProperties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(getUser(), getPassword());
            }
        });
    }

    public boolean sendMail(Conversation conversation, Employee employee, String urlWaveLink) {
        try {
            logger.info("Tentativo di inviare la mail per "+employee.getPCName() + " alla mail " +employee.getEMailAddress());
            // create a message
            Message message = new MimeMessage(mailSession);
            message.setFrom(new InternetAddress(mailAddress));
            message.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse(employee.getEMailAddress()));
            message.setSubject(getSubject(conversation, employee));

            // create and fill the first message part
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText(getBodyMessage(conversation, employee));

            // create the second message part
            MimeBodyPart mbp2 = new MimeBodyPart();

            // attach the file to the message
            if (includeAttachment && urlWaveLink != null && !"".equals(urlWaveLink.trim())) {
                BodyPart attachment = new MimeBodyPart();
                String replace = urlWaveLink.replace("localhost", getWsdlHost());
                URL url = new URL(replace != null ? replace : urlWaveLink);
                mbp2.setDataHandler(new DataHandler(url));
                mbp2.setDisposition(Part.ATTACHMENT);
                mbp2.setFileName(getWaveName(conversation));
            }

            // create the Multipart and add its parts to it
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);

            if (includeAttachment && urlWaveLink != null && !"".equals(urlWaveLink.trim())) {
                mp.addBodyPart(mbp2);
            }

            // add the Multipart to the message
            message.setContent(mp);

            // set the Date: header
            message.setSentDate(new Date());

            Transport.send(message);
            return true;
        } catch (Exception e) {
            getLogger().error("Impossibile inviare la mail", e);
            return false;
        }
    }

    private String getSubject(Conversation conversation, Employee employee) {
        try{
            return "Informazioni registrazione - Ticket id: "+ conversation.getConversationParameters().stream().filter((t) -> {
                    return t.getName().equalsIgnoreCase(customcpTicket);
                }).findFirst().get().getValue();
        }catch(Exception e){
            return "Informazioni registrazione";
        }
    }

    private String getBodyMessage(Conversation conversation, Employee employee) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS a");
        String msg = "Registrazione effettuata\n";
        msg = msg + "Id conversazione: " + conversation.getConversationId() + "\n";
        try {
            GregorianCalendar dateStop = conversation.getDataStart().toGregorianCalendar();
            dateStop.add(Calendar.MILLISECOND, Integer.parseInt("" + conversation.getDuration()));
            final Date startDateTime = conversation.getDataStart().toGregorianCalendar().getTime();
            msg = msg + "Data inizio: " + dateFormatter.format(startDateTime) + "\n";
            msg = msg + "Data fine: " + dateFormatter.format(dateStop.getTime()) + "\n";
            //calcolo durata
            LocalDate startLocalDate = startDateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate stopLocalDate = dateStop.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            Duration between = Duration.between(stopLocalDate, startLocalDate);
            msg = msg + "Durata:" + String.format("%d:%02d:%02d", between.getSeconds() / 3600, (between.getSeconds() % 3600) / 60, (between.getSeconds() % 60)) + "\n";
        } catch (Exception e) {
        }
        try {
            for (int i = 0; i < conversation.getParticipants().size(); i++) {
                int numb = i + 1;
                msg = msg + "Partecipante  " + numb + ": " + conversation.getParticipants().get(i).getPhoneNumber() + "\n";
            }

        } catch (Exception e) {
        }
        msg = msg + "Direzione: " + conversation.getConversationDirection().name() + "\n";
        msg = msg + "\n";
        try {
            msg = msg + "Ticket: " + conversation.getConversationParameters().stream().filter((t) -> {
                return t.getName().equalsIgnoreCase(customcpTicket);
            }).findFirst().get().getValue() + "\n";
        } catch (Exception e) {
        }
        msg = msg + "Nome operatore: " + employee.getFirstName() + " " + employee.getLastName() + "\n";
        msg = msg + "Numero telefonico operatore: " + employee.getPCName() + "\n";
        return msg;
    }

    @Override
    public void fillProperties(Properties prop) {
        this.mailAddress = prop.getProperty("email_address");
        this.user = prop.getProperty("email_user");
        this.password = prop.getProperty("email_password");
        this.mailHost = prop.getProperty("email_host");
        this.mailPort = prop.getProperty("email_port");
        this.wsdlHost = prop.getProperty("db_host");
        this.includeAttachment = "true".equals(prop.getProperty("include_attachment"));
        this.customcpTicket = prop.getProperty("customcp_ticket");
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public Logger getLogger() {
        return logger;
    }

    public String getMailHost() {
        return mailHost;
    }

    public String getMailPort() {
        return mailPort;
    }

    public String getWsdlHost() {
        return wsdlHost;
    }

    public boolean isIncludeAttachment() {
        return includeAttachment;
    }
    
    

    private String getWaveName(Conversation conversation) {
        Optional<ConversationParameter> findFirst = conversation.getConversationParameters().stream().filter((parameter) -> {
            return parameter.getName().equalsIgnoreCase(customcpTicket);
        }).findFirst();
        if (findFirst.isPresent()) {
            return findFirst.get().getValue() + ".mp3";
        }
        return "conversation.wav";
    }

}
