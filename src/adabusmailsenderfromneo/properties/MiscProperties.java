package adabusmailsenderfromneo.properties;

import java.util.Properties;

public class MiscProperties extends CfgProperties {
    private Integer daysForConversations;
    private Boolean sendsMail;

    public MiscProperties(Properties prop) {
        super(prop);
    }

    @Override
    public void fillProperties(Properties prop) {
        this.daysForConversations = Integer.parseInt(prop.getProperty("days_for_conversations"));
        this.sendsMail = "true".equals(prop.getProperty("sends_mail"));
    }


    public Integer getDaysForConversations() {
        return daysForConversations;
    }

    public Boolean getSendsMail() {
        return sendsMail;
    }
}
